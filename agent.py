
from game import Game
from constants import *
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Flatten
from keras import optimizers
from keras import backend as K
import play


class Agent:
    def __init__(self):
        self.model = Sequential()
        self.create_model()

    def create_model(self):
        self.model.add(Conv2D(32, kernel_size=4, strides=(1, 1), input_shape=(8,10,1)))
        self.model.add(Flatten())
        self.model.add(Dense(60, activation='relu'))
        self.model.add(Dense(30, activation='relu'))

        self.model.add(Dense(COL_COUNT))

        sgd = optimizers.SGD(lr=0.0001, momentum=0.5, decay=0.001)

        self.model.compile(loss='mse', optimizer=sgd)

    def train(self):
        for i in range(LEARN_TIMES):
            if i % (LEARN_TIMES / 100) == 0: print(f"Epoch {i}/{LEARN_TIMES}")

            epsilon = EPSILON_INIT + (EPSILON_FINAL - EPSILON_INIT) * i / LEARN_TIMES

            results = play.playGame(epsilon, self.model)
            if results is None: continue
            learning_rate: float = LR_INIT / (1 + (LR_FINAL_FRACTION - 1) * i / LEARN_TIMES)
            K.set_value(self.model.optimizer.lr, learning_rate)
            x_train = results[0][-1]

            y_train = np.zeros(COL_COUNT)
            y_train[results[1][-1]] = WIN_WEIGHT
            y_train = y_train.reshape(1, COL_COUNT)

            self.model.fit(x_train, y_train, verbose=0)
            self.train_on_previous_plays(results[0], results[1], WIN_WEIGHT, DISCOUNT)

            x_train = results[2][-1]

            y_train = np.zeros(COL_COUNT)
            y_train[results[3][-1]] = LOSE_WEIGHT
            y_train = y_train.reshape(1, COL_COUNT)
            self.model.fit(x_train, y_train, verbose=0)

            if i % (LEARN_TIMES / 100) == 0:
                print(f"Right ANS=410, Real={''.join(self.evaluate())}")

        json_model = self.model.to_json()
        with open('model.json', 'w') as f:
            f.write(json_model)
        self.model.save_weights('weights.h5')

    def train_on_previous_plays(self, boards, actions, reward, discount):
        prev_q_val = self.model.predict(boards[-1])
        acts_len = len(actions)
        act_index = acts_len - 2
        while act_index >= 0:
            x_train = boards[act_index]
            rw = discount ** (acts_len - act_index - 1) * reward + GAMMA * np.max(prev_q_val)
            y_train = np.zeros(10)
            y_train[actions[act_index]] = rw
            y_train = y_train.reshape(1, 10)
            self.model.fit(x_train, y_train, batch_size=1, verbose=0)

            prev_q_val = self.model.predict(boards[act_index])

            act_index -= 1

    def evaluate(self):
        t = []
        game1 = Game(8, 10)
        game2 = Game(8, 10)
        game3 = Game(8, 10)

        game1.doMove(1, 1)
        game1.doMove(0, 2)
        game1.doMove(2, 1)
        game1.doMove(6, 2)
        game1.doMove(3, 1)
        game1.doMove(1, 2)
        t.append(int(np.argmax(self.model.predict(game1.board.reshape(1, 8, 10, 1)))))

        game2.doMove(0, 2)
        game2.doMove(1, 1)
        game2.doMove(0, 2)
        game2.doMove(1, 1)
        game2.doMove(6, 2)
        game2.doMove(1, 1)
        game2.doMove(2, 2)
        t.append(int(np.argmax(self.model.predict(game2.board.reshape(1, 8, 10, 1)))))

        game3.doMove(0, 1)
        game3.doMove(0, 2)
        game3.doMove(1, 1)
        game3.doMove(6, 2)
        game3.doMove(6, 1)
        game3.doMove(2, 2)
        game3.doMove(3, 1)
        game3.doMove(1, 2)
        game3.doMove(1, 1)
        game3.doMove(3, 2)
        game3.doMove(2, 1)
        game3.doMove(0, 2)

        t.append(int(np.argmax(self.model.predict(game3.board.reshape(1, 8, 10, 1)))))
        t = list(map(int, t))
        t = list(map(str, t))

        return t
