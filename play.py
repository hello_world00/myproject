from game import Game
import numpy as np


def play_game_1(epsilon, model):
    game = Game(8, 10)

    states_pl1 = []
    acts_pl1 = []

    states_pl2 = []
    acts_pl2 = []

    turn = 1

    winner = 0
    tie = 0
    while not winner and not tie:
        explore = np.random.random() > epsilon
        f_board = game.board.reshape(1, 8, 10, 1)
        if not explore:
            act = np.argmax(model.predict(f_board))
        else:
            act = np.random.randint(8)

        s = game.doMove(act, 2 if 0 == int(turn) else 1)
        if s is None:
            continue
        if turn:  # 1 - First Player, 0 - 2nd Player
            states_pl1.append(f_board)
            acts_pl1.append(act)
        else:
            states_pl2.append(f_board)
            acts_pl2.append(act)

        winner = game.checkWin()

        if winner == -1:
            tie = 1
        elif winner is None:
            turn = not turn

    if winner > 0:
        winner_board_states = states_pl1 if winner == 1 else states_pl2
        loser_board_states = states_pl1 if winner == 2 else states_pl2
        winner_actions = acts_pl1 if winner == 1 else acts_pl2
        loser_actions = acts_pl1 if winner == 2 else acts_pl2
        return winner_board_states, winner_actions, loser_board_states, loser_actions

    return None


def playGame(epsilon, model):
    s = play_game_1(epsilon, model)
    while s is None or s == -1:
        s = play_game_1(epsilon, model)
    return s
