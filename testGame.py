from game import Game



def main():
    game = Game(10,20)
    game.printBoard()

    game_over = False
    turn = 1
    while not game_over:
        if turn:
            col = int(input("Player 1: "))
            if game.is_valid_location(col):
                row = game.get_next_open_row(col)
                game.doMove(row, col, 1)

                if game.checkWin():
                    game_over = game.checkWin()

        else:
            col = int(input("Player 2:"))
            if game.is_valid_location(col):
                row = game.get_next_open_row(col)
                game.doMove(row, col, 2)

                if game.checkWin():
                    game_over = game.checkWin()
        turn = not turn
        game.printBoard()

    print(f"Player {game_over} is win!")






if __name__ == '__main__':
    main()