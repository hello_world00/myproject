import numpy as np


class Game:
    def __init__(self, x, y):
        self.board = np.zeros((x, y))
        self.rowCount = x
        self.columnCount = y

    def checkWin(self):
        for c in range(self.columnCount - 3):
            for r in range(self.rowCount):
                if self.board[r][c] == self.board[r][c + 1] == self.board[r][c + 2] == self.board[r][c + 3] and \
                        self.board[r][c] != 0:
                    return self.board[r][c]
        for c in range(self.columnCount):
            for r in range(self.rowCount - 3):
                if self.board[r][c] == self.board[r + 1][c] == self.board[r + 2][c] == self.board[r + 3][c] and \
                        self.board[r][c] != 0:
                    return self.board[r][c]

        for c in range(self.columnCount - 3):
            for r in range(self.rowCount - 3):
                if self.board[r][c] == self.board[r + 1][c + 1] == self.board[r + 2][c + 2] == self.board[r + 3][
                    c + 3] and \
                        self.board[r][c] != 0:
                    return self.board[r][c]

        for c in range(self.columnCount - 3):
            for r in range(3, self.rowCount):
                if self.board[r][c] == self.board[r - 1][c + 1] == self.board[r - 2][c + 2] == self.board[r - 3][
                    c + 3] and \
                        self.board[r][c] != 0:
                    return self.board[r][c]

        for e in range(self.columnCount):
            for j in range(self.rowCount):
                if self.board[j][e] == 0:
                    return None

        return -1 # Tie

    def printBoard(self):
        print(np.flip(self.board, 0))

    def is_valid_location(self, col):
        return self.board[self.rowCount - 1][col] == 0

    def get_next_open_row(self, col):
        for r in range(self.rowCount):
            if self.board[r][col] == 0:
                return r

    def doMove(self, y, q):
        x = self.get_next_open_row(y)
        if x==None: return None
        self.board[x][y] = q
        return 1



